package ud610;

import javax.swing.JOptionPane;

public class ud610 {
		
	public void exe() {	    
	        int num[]=new int[Integer.parseInt(JOptionPane.showInputDialog("Introduce el tama�o del array "))];
	  
	        rellenarPrimosRandom(num);
	        showArr(num);
	        int pMayor= primoMayor(num);
	        System.out.println("El primo m�s grande entre " +a+ " y " +b+ " es "+pMayor);
	    }
	
	//Pedimos el rango de numeros
	
	static int a = Integer.parseInt(JOptionPane.showInputDialog(null, "Numero mas peque�o deseado"));
	static int b = Integer.parseInt(JOptionPane.showInputDialog(null, "Numero mas grande deseado"));

	
	public static void rellenarPrimosRandom (int lista[]){
		  
	
        int i=0;
   
        while(i<lista.length){     
        	
            int num=((int)Math.floor(Math.random()*(a-b)+b));           
          
            if (esPrimo(num)){              
                lista[i]=num;               
                i++;            
            }       
        }   
    }   
	
	//Metodo para encontrar primo
    public static boolean esPrimo (int num){       
          
        if (num<=1){
            return false;
        }else{	
        	
        	int prueba;         
            int contador=0;
   
            prueba=(int)Math.sqrt(num);
            for (;prueba>1;prueba--){
                if (num%prueba==0){
                    contador+=1;
                }
            }
            return contador < 1;
        }
    }
   
    //muestra en el array
    public static void showArr(int lista[]){
        for(int i=0;i<lista.length;i++){
            System.out.println("En el puesto "+i+" esta el valor "+lista[i]);
        }
    }
     
    
    //Conseguir el mayor primo
    public static int primoMayor(int lista[]){
        int mayor=0;
        for(int i=0;i<lista.length;i++){
            if(lista[i]>mayor){
                mayor=lista[i];
            }
        }
         
        return mayor;
    }
     
}